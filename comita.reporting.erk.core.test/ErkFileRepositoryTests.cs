﻿using NUnit.Framework;
using System.IO;

namespace comita.reporting.erk.core.test
{
    [TestFixture]
    public class ErkFileRepositoryTests
    {
        const string erkDir = @"C:\Users\stanislavt\source\repos\comita.reporting.erk\testing\erkfolders\A";
        const string erkRegResultsDir = @"C:\Users\stanislavt\source\repos\comita.reporting.erk\testing\erkfolders\B";

        
        [TestCase(erkDir,erkRegResultsDir)]        
        public void GetErkRepoInstance_ok(string erkDir, string erkResultDir)
        {
            var _erkRepo = new ErkFileRepository(erkDir, erkResultDir);
            Assert.IsNotNull(_erkRepo);
        }

        [TestCase(erkDir, erkRegResultsDir)]
        public void GetErkList_ok(string erkDir, string erkResultDir)
        {
            var _erkRepo = new ErkFileRepository(erkDir, erkResultDir);
            var erkList = _erkRepo.GetErkListFromResults(); 
            Assert.IsNotNull(erkList);
        }
    }
}
