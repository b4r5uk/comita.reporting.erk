﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace comita.reporting.erk.core.test
{
    [TestFixture]
    class CertificateParserTests
    {
        const string erkDir = @"C:\Users\stanislavt\source\repos\comita.reporting.erk\testing\erkfolders\A";
        
        [TestCase(erkDir)]
        public void ParseErkCert_GetInn_ok(string erkDir)
        {
            var testfile = Directory.GetFiles(erkDir)[10];
            var erkInfo = ErkParser.ParseErkXml(testfile);
            var cert = CertificateParser.ParseErkCert(erkInfo.ErkCertString);
            //todo: finish
            Assert.IsNotNull(cert.CommonName);

        }
    }
}
