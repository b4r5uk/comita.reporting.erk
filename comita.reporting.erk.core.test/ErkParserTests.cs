﻿using NUnit.Framework;
using System.IO;

namespace comita.reporting.erk.core.test
{
    [TestFixture]
    class ErkParserTests
    {
        const string erkDir = @"C:\Users\stanislavt\source\repos\comita.reporting.erk\testing\erkfolders\A";
        const string erkRegResultsDir = @"C:\Users\stanislavt\source\repos\comita.reporting.erk\testing\erkfolders\B";

        [TestCase()]
        public void ParseErk_ok()
        {
            var testfile = Directory.GetFiles(erkDir)[10];
            var xml = ErkParser.ParseErkXml(testfile);
            Assert.IsNotNull(xml.ErkCertString);
            Assert.IsNotNull(xml.ErkAbnType);
            Assert.IsNotNull(xml.ErkCardDate);
            Assert.IsNotNull(xml.ErkName);
        }
    }
}
