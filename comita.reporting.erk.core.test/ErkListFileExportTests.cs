﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace comita.reporting.erk.core.test
{
    [TestFixture]
    class ErkListFileExportTests
    {
        const string erkDir = @"C:\Users\stanislavt\source\repos\comita.reporting.erk\testing\erkfolders\A";
        const string erkRegResultsDir = @"C:\Users\stanislavt\source\repos\comita.reporting.erk\testing\erkfolders\B";

        [Test]
        public void Save2File_ok()
        {
            var repo = new ErkFileRepository(erkDir, erkRegResultsDir);
            List<Erk> erks = repo.GetErkListFromResults();
            string randFilePath = Path.GetTempFileName();

            ErkListFileExport<Erk>.Save2File(erks, randFilePath);

            Process.Start("notepad.exe", randFilePath);
            Assert.IsTrue(File.Exists(randFilePath) & File.ReadAllText(randFilePath).Length > 10);

        }
    }
}
