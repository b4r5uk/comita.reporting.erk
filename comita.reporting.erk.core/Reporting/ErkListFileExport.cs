﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using CsvHelper.Configuration;

namespace comita.reporting.erk.core
{
    public static class ErkListFileExport<T>
    {
        public static void Save2File(List<T> list, string destinationFilePath)
        {
            using (var writer = new StreamWriter(destinationFilePath))
            using (var csv = new CsvWriter(writer))
            {
                csv.Configuration.RegisterClassMap(new ErkMap());
                // todo: align readable
                csv.Configuration.Delimiter = ";";
                csv.WriteRecords(list);                
            }
        }
        
    }
    public class ErkMap : ClassMap<Erk>
    {
        public ErkMap()
        {

            Map(m => m.ErkRegResultFileInfo.Name).Name("RegFileName");
            Map(m => m.ErkRegResultFileInfo.LastWriteTime).Name("RegFileLastWriteDt");
            Map(m => m.ErkFileInfo.Name).Name("ErkFileName");
            Map(m => m.ErkFileInfo.LastWriteTime).Name("ErkFileLastWriteDt");
            Map(m => m.ErkInfo.ErkCardDate).Name("ErkCardDate");
            Map(m => m.ErkInfo.ErkName).Name("ErkAbnName");
            Map(m => m.ErkInfo.InnFromCardId).Name("ErkInn");
            Map(m => m.ErkInfo.ErkAbnType).Name("RegType");
            Map(m => m.ErkInfo.ErkCertInfo.CommonName).Name("ErkCertCN");

            //Map(m => m.ErkInfo.ErkCertInfo.Surname + " " + m.ErkInfo.ErkCertInfo.GivenName).Name("ErkCertFio");
            Map(m => m.ErkInfo.ErkCertInfo.Surname).ConvertUsing(m => m.ErkInfo?.ErkCertInfo?.Surname + " " + m.ErkInfo?.ErkCertInfo?.GivenName).Name("ErkCertFio");
            Map(m => m.ErkInfo.ErkCertInfo.Inn).Name("ErkCertInn");
            Map(m => m.ErkInfo.ErkCertInfo.ValidTo).Name("ErkCertExpireDate");
            
            Map(m => m.ErkRegResultInfo.ResultResult).Name("RegResult");

        }
    }
}
