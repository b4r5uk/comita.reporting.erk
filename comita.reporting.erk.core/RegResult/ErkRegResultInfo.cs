﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace comita.reporting.erk.core
{
    public class ErkRegResultInfo
    {
        /// <summary>
        /// Element = Answer Attribute=AbonentIdentifier
        /// </summary>
        public readonly string ResultAbonentIdentifier;

        /// <summary>
        /// Element = Answer Attribute=ERKName
        /// </summary>
        public readonly string ResultErkName;

        /// <summary>
        /// Element = Result Value
        /// </summary>
        public readonly string ResultResult;

        /// <summary>
        /// If Result Value = ERROR:
        /// Element = ErrorMessage Value
        /// </summary>
        public readonly string ResultErrorMessage;

        public ErkRegResultInfo(string resultAbonentIdentifier, string resultErkName, string resultResult, string resultErrorMessage)
        {
            ResultAbonentIdentifier = resultAbonentIdentifier;
            ResultErkName = resultErkName;
            ResultResult = resultResult;
            ResultErrorMessage = resultErrorMessage;
        }
    }
}
