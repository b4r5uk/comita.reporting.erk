﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace comita.reporting.erk.core
{
    public class ErkCertInfo
    {
        public readonly string CommonName;
        public readonly string Surname;
        public readonly string GivenName;
        public readonly string Inn;
        public readonly DateTime ValidFrom;
        public readonly DateTime ValidTo;

        public ErkCertInfo(string commonName, string surname, string givenName, string inn, DateTime validFrom, DateTime validTo)
        {
            CommonName = commonName;
            Surname = surname;
            GivenName = givenName;
            Inn = inn;
            ValidFrom = validFrom;
            ValidTo = validTo;
        }
    }
}
