﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Org.BouncyCastle.X509;
using Org.BouncyCastle.Asn1.X509;

namespace comita.reporting.erk.core
{
    public static class CertificateParser
    {
        private static X509Certificate GetCertObj(string certB64String)
        {
            Org.BouncyCastle.X509.X509CertificateParser cp = new X509CertificateParser();
            return cp.ReadCertificate(GetByteCertFromString(certB64String));
        }

        private static byte[] GetByteCertFromString(string certB64)
        {
            string certB64Trim = PrepareB64CertStringForImport(certB64);
            return Convert.FromBase64String(certB64Trim);            
        }

        private static string PrepareB64CertStringForImport(string certB64Raw)
        {
            string certStrTrim = certB64Raw.Trim().Replace(System.Environment.NewLine, "");
            string certStrWoHeaders = certStrTrim.Replace("-----BEGIN CERTIFICATE-----", "").Replace("-----END CERTIFICATE-----", "");
            return certStrWoHeaders;
        }
        //todo: validate values
        public static ErkCertInfo ParseErkCert(string certB64String)
        {
            var cert = GetCertObj(certB64String);
            var subj = cert.SubjectDN;
            string commonName = subj.GetValueList(X509Name.CN)[0].ToString();
            string surname = subj.GetValueList(X509Name.Surname)[0].ToString();
            string givenName = subj.GetValueList(X509Name.GivenName)[0].ToString();
            string inn = subj.GetValueList(new Org.BouncyCastle.Asn1.DerObjectIdentifier("1.2.643.3.131.1.1"))[0].ToString();
            DateTime validFrom = cert.NotBefore;
            DateTime validTo = cert.NotAfter;
            var certInfo = new ErkCertInfo(commonName, surname, givenName, inn, validFrom, validTo);
            return certInfo;

        }

    }
}
