﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace comita.reporting.erk.core
{
    public class Erk
    {
        public FileInfo ErkFileInfo;
        public FileInfo ErkRegResultFileInfo;
        
        public ErkInfo ErkInfo;
        public ErkRegResultInfo ErkRegResultInfo;
    }
}
