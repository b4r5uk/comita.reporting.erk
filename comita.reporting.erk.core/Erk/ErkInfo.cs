﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace comita.reporting.erk.core
{
    public class ErkInfo
    {
        /// <summary>
        /// Element=REGCARD Attribute = "CARDDATE"
        /// </summary>
        public readonly string ErkCardDate;

        /// <summary>
        /// Element=REGCARD Attribute = "DEALERID"
        /// </summary>
        public readonly string ErkDealerId;

        /// <summary>
        /// Element=REGCARD Attribute = "CARDID"
        /// </summary>
        public readonly string InnFromCardId;

        /// <summary>
        /// Element=REGCARD Attribute = "NAME"
        /// </summary>
        public readonly string ErkName;


        /// <summary>
        /// Element=MODULE Attribute = "ABN_TYPE"
        /// </summary>
        public readonly string ErkAbnType;

        /// <summary>
        /// Element=MODULE Value
        /// </summary>
        public readonly string ErkCertString;

        public ErkCertInfo ErkCertInfo;

        public ErkInfo(string erkCardDate, 
                        string erkDealerId, 
                        string innFromCardId, 
                        string erkName, 
                        string erkAbnType, 
                        string erkCertString, 
                        ErkCertInfo erkCertInfo)
        {
            ErkCardDate = erkCardDate;
            ErkDealerId = erkDealerId;
            InnFromCardId = innFromCardId;
            ErkName = erkName;
            ErkAbnType = erkAbnType;
            ErkCertString = erkCertString;
            ErkCertInfo = erkCertInfo;
        }
    }
}
