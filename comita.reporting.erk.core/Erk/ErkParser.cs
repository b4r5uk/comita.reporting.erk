﻿using System;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;
using System.Xml.Linq;

namespace comita.reporting.erk.core
{
    public static class ErkParser
    {
        public static ErkInfo ParseErkXml(string erkXmlPath)
        {
            
            XElement regcard = XElement.Load(erkXmlPath);
            var erkCardDate = regcard.Attribute("CARDDATE").Value;
            var erkDealerId = regcard.Attribute("DEALERID").Value;
            var innFromCardId = regcard.Attribute("CARDID").Value;
            var erkName = regcard.Attribute("NAME").Value;

            var moduleEl = regcard.Element("MODULE");
            var erkAbnType = moduleEl.Attribute("ABN_TYPE").Value;

            var certEl = regcard.Element("CERTS").Element("CERT");
            var erkCertString = certEl.Value;
            // todo: test cert parse
            var erkCertInfo = CertificateParser.ParseErkCert(certEl.Value);

            ErkInfo erkInfo = new ErkInfo(erkCardDate, erkDealerId, innFromCardId, erkName, erkAbnType, erkCertString, erkCertInfo);
            return erkInfo;
        }

        public static ErkRegResultInfo ParseErkRegResultXml(string erkRegResultXmlPath)
        {
            XElement answerEl = XElement.Load(erkRegResultXmlPath);

            var resultErkName = answerEl.Attribute("ERKName").Value;
            var resultAbonentIdentifier = answerEl.Attribute("AbonentIdentifier").Value;

            XElement resultEl = answerEl.Element("Result");
            var resultResult = resultEl.Value;
            string resultErrorMessage = null;
            if (resultResult == "ERROR")
            {
                resultErrorMessage = answerEl.Element("ErrorMessage").Value;
            }
            ErkRegResultInfo resultInfo = new ErkRegResultInfo(resultAbonentIdentifier, resultErkName, resultResult, resultErrorMessage);
            return resultInfo;
        }
    }
}
