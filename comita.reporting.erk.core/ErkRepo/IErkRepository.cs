﻿namespace comita.reporting.erk.core
{
    using System.Collections;
    using System.Collections.Generic;

    internal interface IErkRepository
    {
        IList<Erk> GetErk();
    }
}