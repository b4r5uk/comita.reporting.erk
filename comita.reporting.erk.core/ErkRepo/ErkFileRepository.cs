﻿using System.Security.Cryptography.X509Certificates;
using System.Collections.Generic;
using System.IO;
using System;

namespace comita.reporting.erk.core
{
    public class ErkFileRepository
    {
        private readonly string _erkDirPath;
        private readonly string _erkRegResultDirPath;

        private readonly FileInfo[] _erkFileInfos;
        private readonly FileInfo[] _erkRegResultFileInfos;

        public readonly DirectoryInfo ErkDirInfo;
        public readonly DirectoryInfo ErkResultDirInfo;

        public ErkFileRepository(string erkDirPath, string erkRegResultDirPath)
        {
            this._erkDirPath = erkDirPath;
            this._erkRegResultDirPath = erkRegResultDirPath;
            this.ErkDirInfo = new DirectoryInfo(erkDirPath);
            this.ErkResultDirInfo = new DirectoryInfo(erkRegResultDirPath);

            _erkFileInfos = ErkDirInfo.GetFiles();
            _erkRegResultFileInfos = ErkResultDirInfo.GetFiles();
        }


        public List<Erk> GetErkListFromResults()
        {
            List<Erk> _erkList = new List<Erk>();

            foreach (var resultFileXml in _erkRegResultFileInfos)
            {
                Erk erk;

                try
                {
                    erk = new Erk { ErkRegResultFileInfo = resultFileXml, ErkRegResultInfo = ErkParser.ParseErkRegResultXml(resultFileXml.FullName) };
                }
                catch (Exception)
                {
                    continue;
                    //throw;
                }

                string erkFullPath = Path.Combine(_erkDirPath, erk.ErkRegResultInfo.ResultErkName);
                if (!File.Exists(erkFullPath))
                {
                    _erkList.Add(erk);
                    continue;
                }
                else
                {
                    erk.ErkFileInfo = new FileInfo(erkFullPath);
                }
                try
                {
                    erk.ErkInfo = ErkParser.ParseErkXml(erkFullPath);
                }
                catch (Exception)
                {
                    _erkList.Add(erk);
                    continue;
                    //throw;
                }

                _erkList.Add(erk);
            }
            return _erkList;
        }
    }
}
