﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using comita.reporting.erk.core;
using GalaSoft.MvvmLight;


namespace comita.reporting.erk.gui
{
    public class ReportWindowViewModel : ViewModelBase
    {
        public List<Erk> ErkListForReport { get; set; }
        public string FilePath { get; set; }

        public ReportWindowViewModel()
        {
            this.ErkListForReport = SharedViewModel.ErkListShared;
            this.FilePath = ErkListForReport[0].ErkRegResultFileInfo.FullName;
        }


        //private string _filePath = "test FP";
        //public string FilePath
        //{
        //    get { return _filePath; }
        //    set { _filePath = value; RaisePropertyChanged(); }
        //}


    }
}
