﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using comita.reporting.erk.core;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;

namespace comita.reporting.erk.gui
{
    public partial class MainWindowViewModel : ViewModelBase
    {
        private string _erkDirFromTBx = Properties.Settings.Default.ErkDir; //= @"C:\Users\stanislavt\source\repos\comita.reporting.erk\testing\erkfolders\A";
        public string ErkDirFromTBx
        {
            get { return _erkDirFromTBx; }
            set
            {
                _erkDirFromTBx = value.Replace("\"","");
                RaisePropertyChanged();
                ProcessErksCommand.RaiseCanExecuteChanged();

                Properties.Settings.Default.ErkDir = value;
                Properties.Settings.Default.Save();
                Properties.Settings.Default.Reload();
            }
        }

        private string _erkResultDirFromTBx = Properties.Settings.Default.ErkResultDir;
        public string ErkResultDirFromTBx
        {
            get { return _erkResultDirFromTBx; }
            set
            {
                _erkResultDirFromTBx = value.Replace("\"", "");
                RaisePropertyChanged();
                ProcessErksCommand.RaiseCanExecuteChanged();

                Properties.Settings.Default.ErkResultDir = value;
                Properties.Settings.Default.Save();
                Properties.Settings.Default.Reload();
            }
        }
        //ErkReportPath
        private string _erkReportDir = Properties.Settings.Default.ErkReportDir;
        public string ErkReportDir
        {
            get { return _erkReportDir; }
            set
            {
                _erkReportDir = value;
                RaisePropertyChanged();
                ProcessErksCommand.RaiseCanExecuteChanged();
                Properties.Settings.Default.ErkReportDir = value.Replace("\"", "");
                Properties.Settings.Default.Save();
                Properties.Settings.Default.Reload();

            }
        }

        //private void SaveSettings(string varname, string value)
        //{
        //    var s = Properties.Settings.Default;
        //    s.GetType().GetProperty("varname").SetValue(s, value);
        //    //Properties.Settings.Default.ErkReportDir = value;
        //    Properties.Settings.Default.Save();
        //    Properties.Settings.Default.Reload();
        //}
    }
}
