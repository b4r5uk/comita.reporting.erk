﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using comita.reporting.erk.core;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;

namespace comita.reporting.erk.gui
{
    public partial class MainWindowViewModel : ViewModelBase
    {
        private List<Erk> _erkList;
        public List<Erk> ErkList
        {
            get { return _erkList; }
            set
            {
                _erkList = value;
                OpenReportWindowCommand.RaiseCanExecuteChanged();
                RaisePropertyChanged();
            }
        }

        public RelayCommand ProcessErksCommand { get; private set; }
        public RelayCommand OpenReportWindowCommand { get; private set; }
        public RelayCommand GenerateReportCommand { get; private set; }

        public MainWindowViewModel()
        {
            this.ProcessErksCommand = new RelayCommand(ProcessErks, ProcessErksCanExecute);
            this.OpenReportWindowCommand = new RelayCommand(OpenReportWindow, OpenReportWindowCommandCanExecute);
            this.GenerateReportCommand = new RelayCommand(GenerateReport, GenerateReportCommandCanExecute);
        }

        private bool GenerateReportCommandCanExecute()
        {
            if (this._erkList == null)
            {
                return false;
            }
            else if (this._erkList.Count > 0 & !string.IsNullOrEmpty(ErkReportDir))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void GenerateReport()
        {
            if (!Directory.Exists(this.ErkReportDir))
            {
                MessageBox.Show("Sorry, report dir is invalid", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            // RegRep_YYYYMMDDHHMMSS.txt, где
            var sb = new StringBuilder(ErkReportDir).Append("\\RegRep_");
            sb.Append(DateTime.Now.ToString("yyyyMMddHHmmss")).Append(".txt").ToString();            
            string reportFilePath = sb.ToString();
            MessageBox.Show(reportFilePath, "ReportPath", MessageBoxButton.YesNo, MessageBoxImage.Question);
            ErkListFileExport<Erk>.Save2File(ErkList, reportFilePath);
            Process.Start("notepad.exe", reportFilePath);
        }

        private bool OpenReportWindowCommandCanExecute()
        {
            if (this._erkList == null)
            {
                return false;
            }
            else if (this._erkList.Count > 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void OpenReportWindow()
        {
            ReportWindow reportWindow = new ReportWindow();
            reportWindow.Show();
        }

        private bool ProcessErksCanExecute()
        {
            return (!string.IsNullOrEmpty(ErkDirFromTBx) & !string.IsNullOrEmpty(ErkResultDirFromTBx));
        }

        private void ProcessErks()
        {
            if (!(Directory.Exists(ErkDirFromTBx) & Directory.Exists(ErkResultDirFromTBx)))
            {
                MessageBox.Show("Проверьте указанные пути к папкам", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            var repo = new ErkFileRepository(ErkDirFromTBx, ErkResultDirFromTBx);
            this.ErkList = repo.GetErkListFromResults();
            SharedViewModel.ErkListShared = this.ErkList;
            if (_erkList != null & _erkList.Count > 0)
            {
                try
                {
                    // todo: correct if need
                    Analytics_ErkNumTBl = repo.ErkDirInfo.GetFiles().Length;
                    Analytics_ErkResultsNumTBl = repo.ErkResultDirInfo.GetFiles().Length;
                    Analytics_ErkWoResultNumTBl = Analytics_ErkNumTBl - Analytics_ErkResultsNumTBl;
                    Analytics_ResultsWoErkNumTBl = _erkList.Where(e => e.ErkInfo == null).Count();
                    Analytics_MultiResults2SingleErk = _erkList.GroupBy(erk => erk.ErkFileInfo.Name)
                                                                .Where(group => group.Skip(1).Any())
                                                                //.SelectMany(e => e)
                                                                .Count();
                }
                catch (Exception)
                {
                    
                    //throw;
                }
                

            }
        }
    }
}
