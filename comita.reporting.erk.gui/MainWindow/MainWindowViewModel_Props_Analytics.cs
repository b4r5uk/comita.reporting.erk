﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using comita.reporting.erk.core;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;

namespace comita.reporting.erk.gui
{
    public partial class MainWindowViewModel : ViewModelBase
    {
        private int _erkNumTBl;
        public int Analytics_ErkNumTBl
        {
            get { return _erkNumTBl; }
            set { _erkNumTBl = value; RaisePropertyChanged(); }
        }

        private int _erkResultsNumTBl;
        public int Analytics_ErkResultsNumTBl
        {
            get { return _erkResultsNumTBl; }
            set { _erkResultsNumTBl = value; RaisePropertyChanged(); }
        }

        private int _erkWoResultNumTBl;
        public int Analytics_ErkWoResultNumTBl
        {
            get { return _erkWoResultNumTBl; }
            set { _erkWoResultNumTBl = value; RaisePropertyChanged(); }
        }

        private int _resultsWoErkNumTBl;
        public int Analytics_ResultsWoErkNumTBl
        {
            get { return _resultsWoErkNumTBl; }
            set { _resultsWoErkNumTBl = value; RaisePropertyChanged(); }
        }
        private int _analytics_MultiResults2SingleErk;

        public int Analytics_MultiResults2SingleErk
        {
            get { return _analytics_MultiResults2SingleErk; }
            set { _analytics_MultiResults2SingleErk = value; }
        }

    }
}
